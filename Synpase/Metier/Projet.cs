﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Synpase.Metier
{
    class Projet
    {
        private string _nom;
        private DateTime _debut;
        private DateTime _fin;
        private decimal _prixFactureMO;


        private List<Mission> _mission;

        public decimal MargeBruteCourante()
        {
            decimal resultat;
           decimal cumul = this.cumulCoutMO();
            decimal prixFacture = this._prixFactureMO;
            resultat = cumul - prixFacture;
            return resultat;
        }

        private decimal cumulCoutMO()
        {
           decimal resultat = 0;

            foreach (Mission missionCOurante in _listMission)
                {
                resultat = resultat + (missionCOurante.NbHeuresEffectues() * missionCOurante.Intervenant.tauxHoraire);

                }
            return  resultat;
        }
    }
}
