﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Synpase.Metier
{
    class Mission
    {
        private string _nom;
        private string _description;
        private int _nbHeuresPrevues;
        private Dictionary<DateTime, int> _releveHoraire;

        private Intervenant _executant;

        private Intervenant Executant
        {
            get { return _executant; }
        }

        public Dictionary<DateTime, int> releveHoraire
        {
            get { return _releveHoraire; }

        }
        public void AjouteReleve(DateTime date, int nbHeures)
        {
            _releveHoraire.Add(date, nbHeures);
        }

        public int NbHeuresEffectues()
        {
            int resultat = 0;

            foreach (KeyValuePair<DateTime, int > i in _releveHoraire)
            {
                resultat = resultat + i.Value;
            }
            return resultat;
        }
    }
}
